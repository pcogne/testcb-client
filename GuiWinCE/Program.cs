﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CB.Pierre.Test.GuiWinCE
{
    static class Program
    {
        public const Boolean CST_TEST_MODE = true;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            Application.Run(new MainForm());
        }
    }
}