﻿namespace CB.Pierre.Test.GuiWinCE
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_Gpin = new System.Windows.Forms.TextBox();
            this.btn_GetProductInfo = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_ProductName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbl_RequestResult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(14, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(296, 20);
            this.label1.Text = "Enter the 13 digits GPIN";
            // 
            // tb_Gpin
            // 
            this.tb_Gpin.Location = new System.Drawing.Point(14, 41);
            this.tb_Gpin.Name = "tb_Gpin";
            this.tb_Gpin.Size = new System.Drawing.Size(138, 23);
            this.tb_Gpin.TabIndex = 1;
            this.tb_Gpin.Text = "0737628064502";
            // 
            // btn_GetProductInfo
            // 
            this.btn_GetProductInfo.Location = new System.Drawing.Point(14, 70);
            this.btn_GetProductInfo.Name = "btn_GetProductInfo";
            this.btn_GetProductInfo.Size = new System.Drawing.Size(138, 20);
            this.btn_GetProductInfo.TabIndex = 2;
            this.btn_GetProductInfo.Text = "Get Product Info";
            this.btn_GetProductInfo.Click += new System.EventHandler(this.btn_GetProductInfo_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(14, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 20);
            this.label2.Text = "Name";
            // 
            // lbl_ProductName
            // 
            this.lbl_ProductName.Location = new System.Drawing.Point(65, 146);
            this.lbl_ProductName.Name = "lbl_ProductName";
            this.lbl_ProductName.Size = new System.Drawing.Size(181, 20);
            this.lbl_ProductName.Text = "[name]";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(14, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 20);
            this.label3.Text = "Request result :";
            // 
            // lbl_RequestResult
            // 
            this.lbl_RequestResult.Location = new System.Drawing.Point(114, 102);
            this.lbl_RequestResult.Name = "lbl_RequestResult";
            this.lbl_RequestResult.Size = new System.Drawing.Size(38, 20);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(329, 455);
            this.Controls.Add(this.lbl_RequestResult);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbl_ProductName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_GetProductInfo);
            this.Controls.Add(this.tb_Gpin);
            this.Controls.Add(this.label1);
            this.Menu = this.mainMenu1;
            this.Name = "MainForm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_Gpin;
        private System.Windows.Forms.Button btn_GetProductInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_ProductName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbl_RequestResult;
    }
}

