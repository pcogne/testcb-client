﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace CB.Pierre.Test.GuiWinCE
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btn_GetProductInfo_Click(object sender, EventArgs e)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create("https://testcb-server.herokuapp.com/product/" + tb_Gpin.Text);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.KeepAlive = false;
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                string jsonResponse = string.Empty;
                using (StreamReader streamReader = new StreamReader(webResponse.GetResponseStream()))
                {
                    jsonResponse = streamReader.ReadToEnd();
                }
                webResponse.Close();
                lbl_ProductName.Text = jsonResponse;
                lbl_RequestResult.Text = "Success";
            }
            catch (Exception ex)
            {
                lbl_RequestResult.Text = ex.Message;
            }
        }

    }
}